<!--
vim: set fenc=utf-8 ff=unix sts=2 sw=2 et ft=markdown :
-->

[![Build Status][build-status-badge]][build-status-link]

[build-status-badge]: https://gitlab.com/waylang/book/badges/master/pipeline.svg
[build-status-link]: https://gitlab.com/waylang/book/commits/master

# Way
The Definitive Guide

## License

Copyright (C) 2016-2020 Philip H. Smith

"Way: The Definitive Guide" is licensed under a [Creative Commons Attribution-ShareAlike
4.0 International License][cc-by-sa-4.0].

The contained program used to produce "Way: The Definitive Guide" is released under
GPL 3 as stated below:

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

[cc-by-sa-4.0]: http://creativecommons.org/licenses/by-sa/4.0/
